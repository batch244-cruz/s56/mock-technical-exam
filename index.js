function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.


    if (typeof letter === 'string' && letter.length === 1) {
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
            result++;
        }
    }
    return result;
  } else {

    return undefined;
    }
}
    

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // Loop through each letter in the text
    // If the function finds a repeating letter, return false. Otherwise, return true.
    // No repeats found, the text is an isogram
    // The returned value should be a string.

    text = text.toLowerCase();

    for (let x = 0; x < text.length; x++) {    
        for (let y = x + 1; y < text.length; y++) {
            if (text[x] === text[y]) {
            return false;
            }
        }
    }
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.

    if (age < 13) {
        return undefined;
   
    } else if (age >= 13 && age <= 21 || age >= 65) {
        return (price * 0.8).toFixed(2);
    } else {
        return price.toFixed(2);
    }
}


function findHotCategories(items) {
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    
    const hotCategories = [];

    items.forEach((item) => {
        if (item.stocks === 0) {
            if (!hotCategories.includes(item.category)) {
                hotCategories.push(item.category);
            }
        }
    });
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
     // Find voters who voted for both candidate A and candidate B.

    const voters1 = new Set(candidateA);
    const voters2 = new Set(candidateB);
    const flyingVoters = [];

    for (const voter of voters1) {
        if (voters2.has(voter)) {
            flyingVoters.push(voter);
        }
    }
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};